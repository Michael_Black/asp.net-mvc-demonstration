﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using test.Models;

namespace test.Controllers
{
    public class OrganizationController : Controller
    {
        private readonly IOrganizationService _organizationService;


        public OrganizationController(IOrganizationService organizationService)
        {
            _organizationService = organizationService;
        }

        
        public async Task<ActionResult> Index()
        {
            var organizations = await _organizationService.GetOrganizations();
            return View(organizations);
        }

        [HttpPost]
        [Route("AddOrganization")]
        public async Task<ActionResult> AddOrganization(Organization entry)
        {

            await _organizationService.AddOrganization(new Models.Organization() {
                OrganizationName = entry.OrganizationName,
                Founders = entry.Founders.Select(founder => new Founders() {
                    FounderName = founder.FounderName,
                    FounderSurname = founder.FounderSurname,
                    FounderPat = founder.FounderPat,
                    OrganizationName = entry
                }).ToList()

            
            });

            return RedirectToAction("Index");
        }

        [HttpGet]
        [Route("AddOrganization")]
        public ViewResult AddOrganization() => View();


    }
}