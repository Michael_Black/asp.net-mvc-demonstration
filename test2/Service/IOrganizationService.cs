﻿using System.Collections.Generic;
using System.Threading.Tasks;
using test.Models;

namespace test
{
    public interface IOrganizationService
    {
        Task<List<Organization>> GetOrganizations();
        Task AddOrganization(Organization organization);
    }
}