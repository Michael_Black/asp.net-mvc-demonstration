﻿using System.Collections.Generic;
using System.Threading.Tasks;
using test.Models;

namespace test
{
    public interface IOrganizationRepository
    {
        Task<List<Organization>> GetOrganizations();
        Task<Organization> AddOrganization(Organization organization);
    }
}