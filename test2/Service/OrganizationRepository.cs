﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using test.Models;
using Microsoft.Extensions.DependencyInjection;

namespace test
{
    public class OrganizationRepository : IOrganizationRepository
    {
        private IServiceProvider _serviceProvider;

        public OrganizationRepository(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
           // _organizations = new List<Organization> { new Organization {OrganizationName = "Org1", Founders = new List<string> { "Petia", "Vasia","Misha" } }, new Organization { OrganizationName = "Org2", Founders = new List<string> { "Anton", "Max", "Roma","Misha2" } } };
        }

        public async Task<List<Organization>> GetOrganizations()
        {
            using (var scope = _serviceProvider.CreateScope())
            using (var context = scope.ServiceProvider.GetService<OrganizationDatabaseContext>())
            {
                return await Task.Run(() => context.Organization.ToList());
            }
        }

        public async Task<Organization> AddOrganization(Organization organization)
        {
            using (var scope = _serviceProvider.CreateScope())
            using (var context = scope.ServiceProvider.GetService<OrganizationDatabaseContext>())
            {
                var Org = await context.Organization.AddAsync(organization);
                await context.SaveChangesAsync();
                return   Org.Entity;
            }
            
        }
    }
}