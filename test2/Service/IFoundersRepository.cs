﻿using System.Collections.Generic;
using System.Threading.Tasks;
using test.Models;

namespace test2.Service
{
    public interface IFoundersRepository
    {
        Task AddFounders(Organization organization);
        Task<List<Founders>> GetFoundersByOrganizationId(int organizationId);
    }
}