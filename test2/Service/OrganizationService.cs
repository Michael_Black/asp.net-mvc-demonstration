﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using test.Models;
using test2.Service;

namespace test
{
    public class OrganizationService : IOrganizationService
    {

        private  IOrganizationRepository  _organizationRepository;
        private  IFoundersRepository _foundersRepository;

        public OrganizationService(IOrganizationRepository organizationRepository, IFoundersRepository foundersRepository)
        {
            _organizationRepository = organizationRepository;
            _foundersRepository = foundersRepository;
        }

        public async Task<List<Organization>> GetOrganizations()
        {
            var organization = await _organizationRepository.GetOrganizations();
            organization.ForEach(async o => o.Founders = await _foundersRepository.GetFoundersByOrganizationId(o.id));
            return organization;
        }

        public async Task AddOrganization(Organization organization)
        {
            //_foundersRepository.AddFounders(founders)
            var Org =  await _organizationRepository.AddOrganization(organization);
            
            /*foreach (Founders item in organization.Founders)
            {
                item.Id = Org.Id;    
            }*/
            

        }

    }
}