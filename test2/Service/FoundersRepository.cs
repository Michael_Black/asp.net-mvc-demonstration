﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using test.Models;

namespace test2.Service
{
    public class FoundersRepository : IFoundersRepository
    {

        private  IServiceProvider _serviceProvider;

        public FoundersRepository(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task AddFounders(Organization organization)
        {
            using (var scope = _serviceProvider.CreateScope())
            using (var context = scope.ServiceProvider.GetService<OrganizationDatabaseContext>())
            {
                await context.Founders.AddRangeAsync(organization.Founders);
                await context.SaveChangesAsync();
            }

        }

        public async Task<List<Founders>> GetFoundersByOrganizationId(int organizationId)
        {
            using (var scope = _serviceProvider.CreateScope())
            using (var context = scope.ServiceProvider.GetService<OrganizationDatabaseContext>())
            {
                return await context.Founders.Where(founders => founders.OrganizationName.id == organizationId).ToListAsync();
            }
        }
    }
}
