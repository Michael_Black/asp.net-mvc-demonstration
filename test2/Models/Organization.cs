﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace test.Models
{
    public class Organization
    {
        [Key]
        public int id { get; set; }
        //public string inn { get; set; }
        public string OrganizationName { get; set; }

        public virtual List<Founders> Founders { get; set; } = new List<Founders>();

    }
}