﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;


namespace test.Models
{
    public class Founders
    {
        [Key]
        public int Id { get; set; }
        public string FounderName { get; set; }
        public string FounderSurname { get; set; }
        public string FounderPat { get; set; }
        [ForeignKey("Organization")]
        public Organization OrganizationName { get; set; }
    }
}