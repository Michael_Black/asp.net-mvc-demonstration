﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace test.Models
{
    public class OrganizationDatabaseContext : DbContext
    {
        public OrganizationDatabaseContext(DbContextOptions<OrganizationDatabaseContext> options) : base(options)
        {
        }

        public DbSet<Founders> Founders { get; set; }
        public DbSet<Organization> Organization { get; set; }
    }
}